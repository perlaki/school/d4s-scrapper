const fs = require('fs');
const exec = require('child_process').exec;
const puppeteer = require("puppeteer");
const books = require('./books.json');

const AUTH = {
  email: "",
  password: ""
};

function url(book) {
  return `https://a.digi4school.at/ebook/${book.id}/index.html`;
}

function baseHtml(content) {
  return `<body style="margin:0">${content}</body>`;
}

function pageHtml(page, width = 909, height = 1286) {
  return `<object width="${width}" height="${height}"
  style="width:${width}px; height:${height}px;"
  data="${page}/${page}.svg" 
  type="image/svg+xml" id="${page}"></object>`;
}

async function login(ctx) {
  await ctx.goto("https://digi4school.at/login");
  await ctx.evaluate((AUTH) => {
    $.post("/br/xhr/login", AUTH);
  }, AUTH);

  await ctx.goto("https://digi4school.at/ebooks", {
    waitUntil: "networkidle2"
  });
}

async function getLastPage(ctx, url) {
  await ctx.goto(url, {
    waitUntil: "networkidle2"
  });
  return await ctx.evaluate(() => {
    return Number(window.lastpage);
  });
}

async function writeTOC(ctx, book) {
  await ctx.goto(url(book), {
    waitUntil: "networkidle2"
  });
  const bookmarkslist = await ctx.evaluate(() => {
    return window.bookmarks;
  });
  const bookmarks = {}
  for (let b of bookmarkslist) {
    bookmarks[b.title] = b.page;
  }
  return new Promise((res, rej) => {
    fs.writeFile(`${book.name}.json`, JSON.stringify(bookmarks), (err) => {
      if (err) rej(err);
      else res();
    });
  })
}

async function printPages(ctx, book, start = 1, end = 1) {
  await ctx.goto(url(book), {
    waitUntil: "networkidle2"
  });

  let content = "";
  for (let i = start; i <= end; i++) {
    content += pageHtml(i);
  }
  await ctx.setContent(baseHtml(content));
  await ctx.evaluate(() => {
    const objects = [...document.querySelectorAll("object")]
    window.count = objects.length
    for (let o of objects) {
      o.onload = (e) => {
        window.count--;
        console.log(window.count);
      }
    }
  });

  await ctx.waitFor(() => window.count === 0, {
    timeout: 0
  });
  await ctx.pdf({
    path: `${book.name}.pdf`,
    format: "A4"
  });
}

async function printBook(ctx, book) {
  const lastpage = await getLastPage(ctx, url(book));
  // await printPages(ctx, book, 1, lastpage);
  await writeTOC(ctx, book);
  exec(`./addindex.py ${book.name}.pdf ${book.name}.json`);
}

(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    devtools: false
  });
  const ctx = await browser.newPage();

  ctx.on("console", msg => console.log("Pages to go:", msg.text()));

  await ctx.setViewport({
    width: 909,
    height: 1286
  });

  await login(ctx);

  await printBook(ctx, books[0]);

  await browser.close();
})();